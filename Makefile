DOCHANDLE=ESCAPE-WP5-WHITEPAPER
export TEXMFHOME ?= astron-texmf/texmf

$(DOCHANDLE).pdf: main.tex meta.tex changes.tex contents.tex
	xelatex -jobname=$(subst .,_,$(DOCHANDLE)) main.tex
	makeglossaries $(subst .,_,$(DOCHANDLE))
	biber $(subst .,_,$(DOCHANDLE))
	xelatex -jobname=$(subst .,_,$(DOCHANDLE)) main.tex
	xelatex -jobname=$(subst .,_,$(DOCHANDLE)) main.tex


include astron-texmf/make/vcs-meta.make
include astron-texmf/make/changes.make
