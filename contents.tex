\section{Introduction}

Activities in \gls{ESCAPE} \Acrlong{WP} 5 are broadly divided into two major areas.
First, the work package is developing \gls{ESAP}, the \Acrlong{ESAP}: a unified mechanism by which users can discover and interact with the data products, software tools, workflows, and services that are made available through \gls{ESCAPE}.
Second, members of WP5 are preparing their own services, data products, and tools for integration with ESAP and their subsequent use within \gls{ESCAPE} and across \gls{EOSC}.

This document is structured as follows.
\Cref{sec:vision} briefly summarizes the overall vision for the \gls{ESAP} system, describing what is being built, and why it is useful.
The current status of, and major achievements in, \gls{ESAP} construction are described in \cref{sec:achieved}.
Finally, \cref{sec:future} considers possible future directions, including both specific technical work on the existing \gls{ESAP} codebase (\cref{sec:future:technical}) and a consideration of follow-up activities with wider potential impacts (\cref{sec:future:prospects}).

\section{The \gls{ESAP} vision}
\label{sec:vision}

\begin{figure}
\centering
\includegraphics[width=0.66\textwidth]{figures/ESCAPE/esap-interfaces.pdf}
\caption{\gls{ESAP} provides a single, consistent, interface and point of access to a variety of services drawn from a range of providers. Links to the other work packages \gls{ESCAPE} are indicated.}
\label{fig:vision}
\end{figure}

\gls{ESAP} is a \emph{science platform toolkit}: an integrated set of software components which \glspl{ESFRI}, \gls{ESCAPE} project partners, and other groups can use to rapidly assemble and deploy platforms that are customized to the needs of their particular user communities and which integrate their existing service portfolios.
These various deployed instances of \gls{ESAP} then provide the key interfaces between the services delivered by the \gls{ESCAPE} project and the wider scientific community.

In order to meet this goal, \gls{ESAP} has been designed to be flexible and adaptable to the particular needs of each user community.
This is achieved my abstracting the details of heterogeneous underlying infrastructures away from users, a task that is achieved by \gls{ESAP}'s modular, plugin-driven architecture: an \gls{ESAP} instance is integrated with its surrounding environment by enabling and configuring an appropriate selection of plugins, and new capabilities are easily added by writing new plugins.

While this plugin based system makes \gls{ESAP} infinitely re-configurable, its base level of functionality includes:

\begin{itemize}
\item{Data discovery and retrieval from a range of archives and data repositories;}
\item{Exploration and discovery of relevant tools within the ESCAPE software repository;}
\item{Access to a range of compute and analysis services provided both by project partners and by other facilities;}
\item{Orchestration of data, services, and software to help users create and access research environments that meet particular needs.}
\end{itemize}

This model is shown schematically in \cref{fig:vision}, which illustrates the range of services that \gls{ESAP} can help the user access.
The flexibility of this approach means that instances of \gls{ESAP} can be deployed at a variety of scales, from providing services to just a few users within a small project, to supporting major pieces of infrastructure.

\section{Achievements to date}
\label{sec:achieved}

\subsection{Platform capabilities}

The core \gls{ESAP} system --- the user interface, the API Gateway, and a selection of plugins connecting it to key services --- are now mature and well tested.
This includes:

\paragraph{Core data management capabilities} \hfill \\
As the user accumulates and manipulates datasets, they accumulate results and carry them with them through the system.
Subsequent analysis tasks augment this data collection.
Ultimately, it can be persisted for future references and, if appropriate, published for wider use.

\paragraph{Data archive interfaces} \hfill \\
The user is able to use \gls{ESAP} to search for and discover data in a wide range of archives.
While this certainly includes archives which are built upon open standards --- and, in particular, \gls{VO} interfaces as described below --- it is possible to incorporate bespoke and non-standard data sources by writing appropriate plugins.
For example, plugins are currently available that provide access to data collections from Apertif\footnote{\url{https://alta.astron.nl}} and Zooniverse\footnote{\url{https://www.zooniverse.org}}, amongst others.
Note that the \gls{ESAP} interface adapts appropriately depending on the data collection being queried, and even makes it possible to search multiple semantically-related archives simultaneously.

\paragraph{Interactive data analysis facilities} \hfill \\
The user can spawn interactive analysis jobs based on Jupyter\footnote{\url{https://jupyter.org}} running at a range of different computing facilities.
These jobs can integrate with the other \gls{ESCAPE} work packages, as described below.

\paragraph{Data Lake integration (\gls{DIOS}, \gls{WP}2)} \hfill \\
\gls{ESAP} provides direct access to query the Data Lake, discovering data and working with it directly within the core \gls{ESAP} interface.
Further, the \emph{Data Lake as a Service} system provides direct integration of the Data Lake with the interactive analysis environments available through \gls{ESAP}.

\paragraph{Software Repository integration (\gls{OSSR}, \gls{WP}3)} \hfill \\
The \gls{ESCAPE} \gls{OSSR} collects software from across the various \glspl{ESFRI} associated with the project.
Deep integration between \gls{ESAP} and the \gls{OSSR} makes it possible for \gls{ESAP} users to discover this software and dispatch it --- together with their chosen datasets -- directly to selected analysis environments.

\paragraph{Virtual Observatory protocol support (\gls{CEVO}, \gls{WP}4)} \hfill \\
\gls{ESAP} provides pervasive support for \gls{VO} standards, developed in conjunction with \gls{ESCAPE} \gls{WP}4.
Users can locate and access data for processing via \gls{ESAP} using \gls{VO} systems, and even leverage the \gls{IVOA} \gls{SAMP} standard to have \gls{ESAP} communicate with applications running on their personal computer!

\paragraph{Managed database support} \hfill \\
\gls{ESAP}'s (prototype) managed database support makes it possible to fetch results from multiple remote catalogues into a local database that can be used for advanced analytics functionality like cross-matching diverse catalogues.

\subsection{Software packaging and support}

The \gls{WP}5 team have packaged and integrated a wide variety of scientific analysis software, covering a variety of the \gls{ESCAPE} \glspl{ESFRI}, in forms that are designed for re-use and ultimately for integration with the \gls{OSSR}.
Space constraints make it impossible to list all of the various initiatives in this white paper, but illustrative examples include:

\begin{itemize}

\item{Publication of solutions to the \gls{SKA} Science Data Challenge to the \gls{OSSR};}
\item{Packaging and containerization of the R3BRoot and CmbRoot\footnote{\url{https://www.r3broot.gsi.de} and \url{https://redmine.cbm.gsi.de/projects/cbmroot}} tools;}
\item{A reproduction package for the scientific analysis of Hickson Compact Group 16 described in Jones et al. (2021)\footnote{\url{https://ui.adsabs.harvard.edu/abs/2019A\%26A.632A..78J/abstract}};}

\end{itemize}

\subsection{Infrastructure deployment}

Deployment and provisioning of infrastructure for the long term is not within the scope of \gls{WP}5.
However, the work package does support a number of software installations which have provided limited services to end users while acting as proofs-of-concept for the \gls{ESAP} system.
In particular, these include:

\begin{itemize}

\item{\gls{ESAP} core instances deployed at ASTRON and SKAO;}

\item{JupyterHub\footnote{\url{https://jupyter.org/hub}} and/or BinderHub\footnote{\url{https://binderhub.readthedocs.io/}} systems deployed at CSIC, JIV-ERIC, FAIR, and RuG.}

\end{itemize}

These systems have been instrumental both in integrating \gls{ESAP} itself and in testing scientific workflows within the associated user communities.

\section{Future goals}
\label{sec:future}

This section considers future work in the \gls{ESAP} context.
We consider first the natural technical evolution of the codebase --- areas in which technical work beyond that scoped for \gls{ESCAPE} could have substantial impact.
Then we discuss the wider impacts that future support for \gls{ESAP} could have on the \gls{EOSC} and \gls{ESFRI} community.

\subsection{Technical development}
\label{sec:future:technical}

The current \gls{ESAP} system, as described in \cref{sec:achieved}, provides a robust and reasonably fully-featured environment.
However, there are many opportunities for further enhancements that cannot be completed within the scope of \gls{ESCAPE}.
This include:

\begin{itemize}

  \item{Improved support for provenance tracking through the \gls{ESAP} data management system, including robust minting of \glspl{PID} for published results.}
  \item{Support for sharing data between users of the platform, enabling collaborative workflows.}
  \item{Support for persistent development environments, in which users are able to store the state of their session and return to it in future.}
  \item{Richer understanding of the links between data products, enabling features such as presenting science products together with the calibration data used to generate them.}
  \item{Federation between distributed \gls{ESAP} instances.}

\end{itemize}

In addition, \gls{ESAP} remains endlessly adaptable to integrate with new external service offerings.
Many opportunities for these arise: from the management of OpenStack\footnote{\url{https://www.openstack.org}}-based virtual machines to integration with workflow- or function-as-a-service systems.

\subsection{Wider prospects}
\label{sec:future:prospects}

This section addresses the potential wider impacts of \gls{ESAP}: how can continued development of the system continue to further the promise of \gls{EOSC} and \gls{ESCAPE}?

\subsubsection{Development of an \gls{ESCAPE}/\gls{EOSC} \Acrlong{VRE}}

The \gls{ESAP} vision, as described in \cref{sec:vision}, provides the basis for a true \emph{\Acrlong{VRE}}: an online collaborative system that provides all the tools and services that scientists required to conduct high-impact research.
The flexible and scalable design of \gls{ESAP} means that it can evolve into a role in which it provides seamless access to all of the various software and services which are developed by \gls{ESCAPE}, by its successors, and across the whole \gls{EOSC} ecosystem.
However, assuming such a role requires two major ongoing activities.

First, \gls{ESAP} will require continued maintenance and integration effort.
The platform is stable, solid and extensible, but --- as with any network connected service --- emergent issues will need to be resolved as they arrive.
Further, while the plugin-driven \gls{ESAP} system is extensible to address a wide range of use cases, development effort will be required to integrate new service plugins and to extend the \gls{ESAP} core to support new service types when necessary.

Secondly, the \gls{ESCAPE} project provides no long-term structural support for delivery of \gls{ESAP} as an operational service.
Within the context of the project, various \gls{ESCAPE} partners have deployed infrastructure in support of \gls{ESAP} development or in support of particular \gls{ESFRI} use cases.
However, these are inappropriate for providing high-availability services to a more general community: that would require not only dedicated infrastructure, but also support and maintenance services.

\subsubsection{Sustaining \gls{ESAP} in support of \gls{ESFRI} development}

Several of the \gls{ESCAPE}-affiliated \glspl{ESFRI} are currently at a stage in their development where they are making strategic choices about the long-term delivery of services to end users.
For example, the \gls{CTAO} is currently evaluating how to provide user-facing services in the context of its upcoming \emph{Science Data Challenge}, while \gls{SKA} is in the processing of beginning prototype development for its network of \emph{Science Regional Centres}, which will be observatory's primary means of delivering data to end users.
Both of these infrastructures have been heavily involved in \gls{ESCAPE} in general and in the development of \gls{ESAP} in particular; it would be natural for \gls{ESAP} technologies to play a major role in their future plans.

No individual \gls{ESFRI} will find it advantageous to assume responsibility for a legacy software system, even if that system provides useful functionality.
The value of \gls{ESAP} to these projects is therefore dramatically increased if it is not simply a collection of source code, but an actively maintained and supported project with a clear governance model and a sustainable future.


\subsubsection{Common standards for science platform development and interoperability}

The last several years have seen an explosion of heterogeneous development in the field of ``science platforms'' (broadly defined).
Tens or hundreds of projects generally providing some form of interactive analysis environment with access to bulk storage and computing systems are being rolled out across a multitude of research domains\footnote{For example, consider \href{https://github.com/opencadc/skaha}{CANFAR Skaha}, \href{https://data.lsst.cloud}{Rubin Science Platform}, \href{https://datalabs.esa.int}{ESA Datalabs}, \href{https://datalab.noirlab.edu}{NOIRLab Astro Data Lab}, \href{https://www.sciserver.org}{SciServer}, and many others.}.
This ecosystem is disjointed and fragmented: although many of these projects are individually very technically advanced and provide compelling functionality, they are specialized and difficult to apply in other domains.

The \gls{ESAP} experience suggests a way to move beyond this fragmentation of platforms into disconnected systems serving individual communities or infrastructures.
Instead, \gls{ESAP} proposes a model of common standards and interconnectedness among science platform efforts.
By using \gls{ESAP}, individual projects or communities which need particular capabilities can build on a common, interoperable technological basis, customizing and extending it to address just their particular needs.

Moving beyond that: as more platforms become centred around common technical standards, we can move to adopt common standards for access not only to data --- as pioneered by the \Acrlong{VO} in the \gls{ESCAPE} context --- but to compute and other resources.
This process has already started: members of the \gls{ESCAPE} team have engaged with our colleagues in the \gls{IVOA} to begin exploring concepts for the \emph{ExecutionPlanner} system\footnote{\url{https://github.com/ivoa/ExecutionPlannerNote}}, which would standardize access to computing capabilities in much the same way as \gls{IVOA} recommendations harmonize access to data.

Ultimately, by building upon the experience developed in \gls{ESAP} and \gls{CEVO}, one can imagine working towards a future federated network of science platforms, build on common standards, and providing the lowest possible barrier to entry to new service or data providers making their offerings available to a wide range of researchers.
